#ifndef ASYNCMSG_DETAIL_HEADER_H
#define ASYNCMSG_DETAIL_HEADER_H

#include <array>
#include <cstdint>

// byteswap.h and endian.h are GLIBC extensions
#include <endian.h>
#if __BYTE_ORDER == __LITTLE_ENDIAN
#  ifndef htole32
#    define htole32(x) (x)
#  endif
#  ifndef le32toh
#    define le32toh(x) (x)
#  endif
#else
#  include <byteswap.h>
#  ifndef htole32
#    define htole32(x) bswap_32 (x)
#  endif
#  ifndef le32toh
#    define le32toh(x) bswap_32 (x)
#  endif
#endif

namespace daq
{
namespace asyncmsg
{
namespace detail
{

class Header
{

public:

  static const std::size_t SIZE = 12;

  Header()
  {
    m_data.fill(0);
  }

  Header(std::uint32_t typeIdValue, std::uint32_t transactionIdValue, std::uint32_t sizeValue)
  {
    fill(typeIdValue, transactionIdValue, sizeValue);
  }

  const std::array<std::uint8_t, 12>& data() const
  {
    return m_data;
  }

  std::array<std::uint8_t, 12>& data()
  {
    return m_data;
  }

  std::uint32_t typeId() const
  {
    return le32toh(*reinterpret_cast<const std::uint32_t*>(&m_data[0]));
  }

  void typeId(std::uint32_t value)
  {
    *reinterpret_cast<std::uint32_t*>(&m_data[0]) = htole32(value);
  }

  std::uint32_t transactionId() const
  {
    return le32toh(*reinterpret_cast<const std::uint32_t*>(&m_data[4]));
  }

  void transactionId(std::uint32_t value)
  {
    *reinterpret_cast<std::uint32_t*>(&m_data[4]) = htole32(value);
  }

  std::uint32_t size() const
  {
    return le32toh(*reinterpret_cast<const std::uint32_t*>(&m_data[8]));
  }

  void size(std::uint32_t value)
  {
    *reinterpret_cast<std::uint32_t*>(&m_data[8]) = htole32(value);
  }

  void fill(std::uint32_t typeIdValue, std::uint32_t transactionIdValue, std::uint32_t sizeValue)
  {
    typeId(typeIdValue);
    transactionId(transactionIdValue);
    size(sizeValue);
  }

private:

  std::array<std::uint8_t, SIZE> m_data;

};

} // namespace detail
} // namespace asyncmsg
} // namespace daq

#endif // !defined(ASYNCMSG_DETAIL_HEADER_H)
