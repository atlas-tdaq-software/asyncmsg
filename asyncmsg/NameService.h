// this is -*- c++ -*-
#ifndef ASYNCMSG_NAMESERVICE_H_
#define ASYNCMSG_NAMESERVICE_H_

#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <cstdint>
#include "boost/asio.hpp"

#include "ipc/partition.h"
#include "ers/Issue.h"

namespace daq {


    ERS_DECLARE_ISSUE(asyncmsg,
                      Issue, ERS_EMPTY, ERS_EMPTY)

    ERS_DECLARE_ISSUE_BASE(asyncmsg,
                           CannotPublish,
                           Issue,
                           "Cannot publish: no local network interfaces match data networks",
                           ERS_EMPTY, 
                           ERS_EMPTY)

    ERS_DECLARE_ISSUE_BASE(asyncmsg,
		           InvalidISServer,
			   Issue,
			   "Cannot publish because of invalid IS server: " << m_server,
			   ERS_EMPTY,
			   ((std::string)m_server)
                           )

    ERS_DECLARE_ISSUE_BASE(asyncmsg,
                           CannotResolve,
                           Issue,
                           "Cannot resolve name: " << m_name,
                           ERS_EMPTY,
                           ((std::string)m_name)
                           )

    ERS_DECLARE_ISSUE_BASE(asyncmsg,
                           CannotAllocateMulticast,
                           Issue,
                           "Cannot allocate multicast address",
                           ERS_EMPTY,
                           ERS_EMPTY)

    namespace asyncmsg {

        
        /**
         * Helper class to access the message passing name service.
         *
         * It provides both a method to publish the local network information
         * as well as methods to lookup and identify a matching remote network 
         * port.
         */
        class NameService {
        public:

            /**
             * \param[in] partition The IPC partition object.
             * \param[in] data_networks A list of networks in the format network/netmask. Both IP 4 and 6 are accepted.
             * \param[in] is_server The IS server to publish the information to. If the DF_CONFIG_IS_SERVER environment variable
                                    is set, it takes precedence.
             */
            NameService(IPCPartition partition ,
                        const std::vector<std::string>& data_networks, 
                        const std::string& is_server = "DFConfig") noexcept;
            ~NameService() noexcept;

            /** \brief Publish IS object with all valid local interfaces and port number.
             *
             * \param[in] name A string used build the name of the object in IS. Typically the application name.
             * \param[in] port The port number to publish.
             *
             * \throws daq::asyncmsg::CannotPublish, IS exceptions.
             */ 
            void publish(const std::string& name, uint16_t port);
                
            /** \brief Look up all applications starting with the given 'prefix' and return a list of endpoints.
             *
             * \param[in] prefix The search criteria in IS will be 'prefix.*'.
             *
             * \returns A list of boost ASIO TCP endpoints, exactly one for each remote application.
             *
             * \throws IS exceptions
             */
            std::vector<boost::asio::ip::tcp::endpoint> lookup(const std::string& prefix) const;

            /** \brief Look up all applications starting with the given 'prefix' and return a map of names and endpoints.
             *
             * \param[in] prefix The search criteria in IS will be 'prefix.*'.
             *
             * \returns A map of application names and boost ASIO TCP endpoints, exactly one for each remote application.
             *
             * \throws IS exceptions
             */
            std::map<std::string,boost::asio::ip::tcp::endpoint> lookup_names(const std::string& prefix) const;

            /** \brief Look up all applications starting with the given 'prefix' and return a map of names and all endpoints.
             *
             * \param[in] prefix The search criteria in IS will be 'prefix.*'.
             *
             * \returns A map of application names and a list of all boost ASIO TCP endpoints that would be available.
             *
             * \throws IS exceptions
             */            
            std::map<std::string,std::vector<boost::asio::ip::tcp::endpoint>> lookup_all(const std::string& prefix) const;

            /** \brief Resolve a single specific application.
             *
             * \param[in] name Resolves exactly one specific remote applications with this name.
             *
             * \throws daq::asyncmsg::CannotResolve, IS exceptions
             */
            boost::asio::ip::tcp::endpoint resolve(const std::string& name) const;

            /**
             * \brief Allocate the multicast address for the partition.
             *
             * \param[in] addr The string representation of the address; can be a full IP address or '*'.
             * \param[in] network The string representation of the network to use for multicast.
             * \returns The allocated multicast address.
             * \throws daq::asyncmsg::CannotAllocateMulticast, daq::asyncmsg::InvalidISServer
             *
             * Only one application per partition should call this. If the 'addr' parameter contains
             * a '*', the multicast address will be dynamically allocated in such a way that it
             * is unique per installation and across partitions (e.g. in Point 1).
             */
            std::string allocate_multicast_address(const std::string& addr, const std::string& network);

            /** \brief Find the multicast address for the partition.
             * 
             * \returns A string containing the IP addresses usable by boost::asio.
             *
             * \throws daq::asyncmsg::CannotResolve
             *
             * Applications who need the multicast address of the partition should call this with the 
             * 'addr' taken from OKS. If 'addr' is '*' the multicast address will be dynamically looked up.
             */
            std::string lookup_multicast_address(const std::string& addr) const;

            /** typedef for readability, a 2-tuple of IP addresses representing network/netmask. */
            typedef std::tuple<boost::asio::ip::address,boost::asio::ip::address> Network;

            /** \brief A helper method to parse a string of the form network/netmask.
             */
            static Network parse_address_network(const std::string& network);

            /** \brief A helper method to find the local interface matching netmask.
             */
            static boost::asio::ip::address find_interface(const std::string& netmask);

        private:

            bool matches(const boost::asio::ip::address& addr, const boost::asio::ip::address& mask) const;

            IPCPartition         m_partition;
            std::string          m_is_server;
            std::vector<Network> m_data_networks;
        };

    }
}

#endif // ASYNCMSG_NAMESERVICE_H_
