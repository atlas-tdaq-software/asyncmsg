#ifndef ASYNCMSG_ASYNCMSG_H
#define ASYNCMSG_ASYNCMSG_H

namespace daq
{
namespace asyncmsg
{

/*! \page daq_asyncmsg asyncmsg documentation
 *
 *  [TOC]
 *
 *  [asio]: http://www.boost.org/doc/libs/1_50_0/doc/html/boost_asio.html
 *  [asio.io_service]: http://www.boost.org/doc/libs/1_50_0/doc/html/boost_asio/reference/io_service.html
 *
 *  The asyncmsg library provides a generic TCP-based messaging service with asynchronous
 *  semantics. It is based on the [Boost.Asio][asio] library.
 *
 *  Using asyncmsg                                                            {#daq_asyncmsg_using}
 *  ==============
 *
 *  Anatomy                                                           {#daq_asyncmsg_using_anatomy}
 *  -------
 *
 *  This section gives a general picture of the interactions between the library and a program
 *  using it.
 *
 *  ### I/O objects
 *
 *  The library provides two abstract classes that are used to perform I/O operations:
 *  - a messaging session with a remote peer is represented by the \ref Session abstract class;
 *  - a server accepting new incoming sessions is represented by the \ref Server class.
 *
 *  For each I/O operation, the I/O classes define:
 *   - a concrete "initiator method" to initiate the operation (e.g. the Session::asyncOpen()
 *     method);
 *   - an abstract "completion method" that is called when the operation has completed successfully
 *     (e.g. Session::onOpen());
 *   - an abstract "completion method" that is called instead if an error has occurred (e.g.
 *     Session::onOpenError())
 *
 *  A program using asyncmsg should define one or more concrete I/O classes, inheriting from the
 *  appropriate abstract I/O class, that implement the program-specific operation completion
 *  handling logic.
 *
 *  I/O objects *must* always live on the heap, and be managed by a std::shared_ptr.
 *
 *  ### boost::asio::io_service
 *
 *  Being based on Boost.Asio, asyncmsg relies on the [boost::asio::io_service][asio.io_service]
 *  class. An io_service object represents the program's link to the operating system's I/O
 *  services. All I/O objects need access to an io_service instance.
 *
 *  I/O operations are performed asynchronously by the operating system. However, the program must
 *  make a call to io_service::run() in order to retrieve the results of the operations and invoke
 *  the corresponding completion methods. A call to io_service::run() blocks while there are
 *  unfinished asynchronous operations.
 *
 *  ### Example
 *
 *  As an example, let's consider the steps an application needs to take to open a new client
 *  session with a remote peer. We assume here that the application defined a concrete Session
 *  I/O class named MySession.
 *
 *  1. The program needs at least one io_service object.
 *     ~~~{.cxx}
 *     boost::asio::io_service ioService;
 *     ~~~
 *  2. To perform I/O operations the program needs an I/O object such as a concrete Session instance:
 *     ~~~{.cxx}
 *     auto session = std::make_shared<MySession>(io_service);
 *     ~~~
 *  3. The program initiates the open operation by calling the I/O object:
 *     ~~~{.cxx}
 *     using namespace boost::asio::ip;
 *
 *     tcp::endpoint remoteEndpoint(address::from_string("127.0.0.1"), 1234);
 *     std::string localName("MyExampleApp");
 *
 *     session->asyncOpen(localName, remoteEndpoint);
 *     ~~~
 *  4. The asyncmsg library initiates the open operation, using the io_service.
 *  5. If it wasn't doing so already, the program must make a call to io_service::run() for the
 *     operation to complete.
 *  6. While inside the io_service::run() call, the asyncmsg library invokes the appropriate
 *     completion method (MySession::onOpen() or MySession::onOpenError()).
 *
 *
 *  Concurrency                                                   {#daq_asyncmsg_using_concurrency}
 *  -----------
 *
 *  TODO: detail the synchronization issues that arise from the asynchronous nature of the library
 *  and how they are dealt with using boost::asio::io_service::strand
 *
 *
 *  Object life-cycle                                               {#daq_asyncmsg_using_lifecycle}
 *  -----------------
 *
 *  TODO: explain why I/O objects must be managed by std::shared_ptr.
 *
 *
 */

} // namespace asyncmsg
} // namespace daq

#include "asyncmsg/Error.h"
#include "asyncmsg/Session.h"
#include "asyncmsg/Server.h"

#endif // !defined(ASYNCMSG_ASYNCMSG_H)
